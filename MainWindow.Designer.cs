﻿namespace DeckViewer
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.inGameLableDesc = new System.Windows.Forms.Label();
            this.inGameLable = new System.Windows.Forms.Label();
            this.hearthstoneProcessStartedLableDesc = new System.Windows.Forms.Label();
            this.hearthstoneProcessStartedLable = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deckComboBox = new System.Windows.Forms.ComboBox();
            this.deckGroupBox = new System.Windows.Forms.GroupBox();
            this.newButton = new System.Windows.Forms.Button();
            this.deckGridView = new System.Windows.Forms.DataGridView();
            this.deckDeleteButton = new System.Windows.Forms.Button();
            this.name = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.mana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.left = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.readMebutton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.deckGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deckGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // inGameLableDesc
            // 
            this.inGameLableDesc.AutoSize = true;
            this.inGameLableDesc.Location = new System.Drawing.Point(23, 99);
            this.inGameLableDesc.Name = "inGameLableDesc";
            this.inGameLableDesc.Size = new System.Drawing.Size(97, 26);
            this.inGameLableDesc.TabIndex = 0;
            this.inGameLableDesc.Text = "In game:";
            // 
            // inGameLable
            // 
            this.inGameLable.AutoSize = true;
            this.inGameLable.Location = new System.Drawing.Point(202, 99);
            this.inGameLable.Name = "inGameLable";
            this.inGameLable.Size = new System.Drawing.Size(111, 26);
            this.inGameLable.TabIndex = 1;
            this.inGameLable.Text = "scanning..";
            // 
            // hearthstoneProcessStartedLableDesc
            // 
            this.hearthstoneProcessStartedLableDesc.AutoSize = true;
            this.hearthstoneProcessStartedLableDesc.Location = new System.Drawing.Point(23, 53);
            this.hearthstoneProcessStartedLableDesc.Name = "hearthstoneProcessStartedLableDesc";
            this.hearthstoneProcessStartedLableDesc.Size = new System.Drawing.Size(169, 26);
            this.hearthstoneProcessStartedLableDesc.TabIndex = 2;
            this.hearthstoneProcessStartedLableDesc.Text = "Process started:";
            // 
            // hearthstoneProcessStartedLable
            // 
            this.hearthstoneProcessStartedLable.AutoSize = true;
            this.hearthstoneProcessStartedLable.Location = new System.Drawing.Point(202, 53);
            this.hearthstoneProcessStartedLable.Name = "hearthstoneProcessStartedLable";
            this.hearthstoneProcessStartedLable.Size = new System.Drawing.Size(111, 26);
            this.hearthstoneProcessStartedLable.TabIndex = 3;
            this.hearthstoneProcessStartedLable.Text = "scanning..";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.inGameLableDesc);
            this.groupBox1.Controls.Add(this.hearthstoneProcessStartedLable);
            this.groupBox1.Controls.Add(this.hearthstoneProcessStartedLableDesc);
            this.groupBox1.Controls.Add(this.inGameLable);
            this.groupBox1.Location = new System.Drawing.Point(7, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(523, 158);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hearthstone Status";
            // 
            // deckComboBox
            // 
            this.deckComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.deckComboBox.FormattingEnabled = true;
            this.deckComboBox.Location = new System.Drawing.Point(18, 38);
            this.deckComboBox.Name = "deckComboBox";
            this.deckComboBox.Size = new System.Drawing.Size(315, 33);
            this.deckComboBox.TabIndex = 6;
            this.deckComboBox.SelectedIndexChanged += new System.EventHandler(this.deckComboBox_SelectedIndexChanged);
            // 
            // deckGroupBox
            // 
            this.deckGroupBox.Controls.Add(this.newButton);
            this.deckGroupBox.Controls.Add(this.deckGridView);
            this.deckGroupBox.Controls.Add(this.deckDeleteButton);
            this.deckGroupBox.Controls.Add(this.deckComboBox);
            this.deckGroupBox.Location = new System.Drawing.Point(7, 176);
            this.deckGroupBox.Name = "deckGroupBox";
            this.deckGroupBox.Size = new System.Drawing.Size(706, 1194);
            this.deckGroupBox.TabIndex = 7;
            this.deckGroupBox.TabStop = false;
            this.deckGroupBox.Text = "Deck";
            // 
            // newButton
            // 
            this.newButton.AutoSize = true;
            this.newButton.Location = new System.Drawing.Point(365, 35);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(146, 36);
            this.newButton.TabIndex = 11;
            this.newButton.Text = "New";
            this.newButton.UseVisualStyleBackColor = true;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // deckGridView
            // 
            this.deckGridView.AllowUserToOrderColumns = true;
            this.deckGridView.AllowUserToResizeColumns = false;
            this.deckGridView.AllowUserToResizeRows = false;
            this.deckGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.deckGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.deckGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deckGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.deckGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.count,
            this.mana,
            this.left});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.916231F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.deckGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.deckGridView.Enabled = false;
            this.deckGridView.GridColor = System.Drawing.Color.Gainsboro;
            this.deckGridView.Location = new System.Drawing.Point(18, 92);
            this.deckGridView.Name = "deckGridView";
            this.deckGridView.RowHeadersWidth = 25;
            this.deckGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.deckGridView.RowTemplate.Height = 33;
            this.deckGridView.Size = new System.Drawing.Size(672, 1085);
            this.deckGridView.TabIndex = 10;
            this.deckGridView.Visible = false;
            this.deckGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.deckGridView_CellValueChanged);
            this.deckGridView.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.deckGridView_DefaultValuesNeeded);
            this.deckGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.deckGridView_RowsRemoved);
            // 
            // deckDeleteButton
            // 
            this.deckDeleteButton.AutoSize = true;
            this.deckDeleteButton.Enabled = false;
            this.deckDeleteButton.Location = new System.Drawing.Point(542, 35);
            this.deckDeleteButton.Name = "deckDeleteButton";
            this.deckDeleteButton.Size = new System.Drawing.Size(146, 36);
            this.deckDeleteButton.TabIndex = 8;
            this.deckDeleteButton.Text = "Delete";
            this.deckDeleteButton.UseVisualStyleBackColor = true;
            this.deckDeleteButton.Click += new System.EventHandler(this.deckDeleteButton_Click);
            // 
            // name
            // 
            this.name.DisplayStyleForCurrentCellOnly = true;
            this.name.FillWeight = 160F;
            this.name.HeaderText = "Card Name";
            this.name.MinimumWidth = 160;
            this.name.Name = "name";
            this.name.Sorted = true;
            this.name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.name.Width = 160;
            // 
            // count
            // 
            this.count.DisplayStyleForCurrentCellOnly = true;
            this.count.HeaderText = "Total";
            this.count.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.count.Name = "count";
            this.count.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.count.Width = 50;
            // 
            // mana
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.mana.DefaultCellStyle = dataGridViewCellStyle1;
            this.mana.HeaderText = "Mana";
            this.mana.MaxInputLength = 2;
            this.mana.Name = "mana";
            this.mana.ReadOnly = true;
            this.mana.Width = 50;
            // 
            // left
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.left.DefaultCellStyle = dataGridViewCellStyle2;
            this.left.HeaderText = "Left";
            this.left.MaxInputLength = 1;
            this.left.Name = "left";
            this.left.ReadOnly = true;
            this.left.Width = 50;
            // 
            // readMebutton
            // 
            this.readMebutton.AutoSize = true;
            this.readMebutton.Location = new System.Drawing.Point(550, 125);
            this.readMebutton.Name = "readMebutton";
            this.readMebutton.Size = new System.Drawing.Size(146, 43);
            this.readMebutton.TabIndex = 8;
            this.readMebutton.Text = "Read me!";
            this.readMebutton.UseVisualStyleBackColor = true;
            this.readMebutton.Click += new System.EventHandler(this.readMebutton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(747, 1382);
            this.Controls.Add(this.readMebutton);
            this.Controls.Add(this.deckGroupBox);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Hearthstone Deck Viewer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.deckGroupBox.ResumeLayout(false);
            this.deckGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deckGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label inGameLableDesc;
        private System.Windows.Forms.Label inGameLable;
        private System.Windows.Forms.Label hearthstoneProcessStartedLableDesc;
        private System.Windows.Forms.Label hearthstoneProcessStartedLable;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox deckComboBox;
        private System.Windows.Forms.GroupBox deckGroupBox;
        private System.Windows.Forms.Button deckDeleteButton;
        private System.Windows.Forms.DataGridView deckGridView;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.DataGridViewComboBoxColumn name;
        private System.Windows.Forms.DataGridViewComboBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn mana;
        private System.Windows.Forms.DataGridViewTextBoxColumn left;
        private System.Windows.Forms.Button readMebutton;

    }
}

