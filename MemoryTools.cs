﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Threading;

namespace DeckViewer
{
    public static class MemoryTools
    {
        const int PROCESS_QUERY_INFORMATION = 0x0400;
        const int MEM_COMMIT = 0x00001000;
        const int PAGE_READWRITE = 0x04;
        const int PROCESS_WM_READ = 0x0010;
        const int MEM_PRIVATE = 0x20000;

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        [DllImport("kernel32.dll")]
        static extern void GetSystemInfo(out SYSTEM_INFO lpSystemInfo);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int VirtualQueryEx(IntPtr hProcess, IntPtr lpAddress, out MEMORY_BASIC_INFORMATION lpBuffer, uint dwLength);

        struct MEMORY_BASIC_INFORMATION
        {
            public int BaseAddress;
            public int AllocationBase;
            public int AllocationProtect;
            public int RegionSize;
            public int State;
            public int Protect;
            public int lType;
        }

        struct SYSTEM_INFO
        {
            public ushort processorArchitecture;
            ushort reserved;
            public uint pageSize;
            public IntPtr minimumApplicationAddress;
            public IntPtr maximumApplicationAddress;
            public IntPtr activeProcessorMask;
            public uint numberOfProcessors;
            public uint processorType;
            public uint allocationGranularity;
            public ushort processorLevel;
            public ushort processorRevision;
        }

        static bool isCard(byte[] buffer, int from, int to)
        {
            for (int i = from; i < to - 10; ++i)
            {
                if (
                    buffer[i + 0] == 0x00 && buffer[i + 1] == 0x00 &&
                    buffer[i + 2] == 0x00 && buffer[i + 3] == 0x12 &&
                    buffer[i + 4] == 0x00 && buffer[i + 5] == 0xA2 &&
                    buffer[i + 6] == 0xDF && buffer[i + 7] == 0x01 &&
                    buffer[i + 8] == 0x00 && buffer[i + 9] == 0x23 &&
                    buffer[i + 10] == 0x00
                    )
                {
                    return true;
                }
            }

            return false;
        }

        static int getCardStart(byte[] buffer, int from)
        {
            for (int i = from; i > from - 160; --i)
            {
                if (buffer[i] == 0xFD && buffer[i + 1] == 0xFD && buffer[i + 2] == 0xFD && buffer[i + 3] == 0xFD)
                {
                    return i + 4;
                }
            }

            return 0;
        }

        static int getCardEnd(byte[] buffer, int from)
        {
            for (int i = from; i < from + 160; ++i)
            {
                if (buffer[i] == 0xFD && buffer[i + 1] == 0xFD && buffer[i + 2] == 0xFD && buffer[i + 3] == 0xFD)
                {
                    return i;
                }
            }

            return 0;
        }

        static int findSymbol(byte[] buffer, int symbol, int from, int to)
        {
            for (int i = from; i < to; ++i)
            {
                if (buffer[i] == symbol)
                {
                    return i;
                }
            }

            return -1;
        }

        static string getTextString(byte[] buffer, int from, int to)
        {
            for (int i = from; i < to; ++i)
            {
                if (buffer[i] < 0x20 || buffer[i] > 0x7E)
                {
                    return "";
                }
            }

            return System.Text.Encoding.UTF8.GetString(buffer, from, to - from);
        }

        static Card extractCard(byte[] buffer, int position)
        {
            var from = getCardStart(buffer, position);
            var to = getCardEnd(buffer, position);

            if (!isCard(buffer, from, to))
            {
                return null;
            }

            var begin = findSymbol(buffer, 0x5B, from, to);
            var end = findSymbol(buffer, 0x5D, begin, to);

            if (begin == -1 || end == -1)
            {
                return null;
            }

            var cardString = getTextString(buffer, begin + 1, end);

            if (cardString.Length == 0)
            {
                return null;
            }

            Card card = new Card();

            foreach (var field in cardString.Split(' '))
            {
                var kv = field.Split('=');

                if (kv.Length != 2)
                {
                    continue;
                }

                switch (kv[0])
                {
                    case "id":
                        card.id = int.Parse(kv[1]);
                        break;

                    case "cardId":
                        card.cardId = kv[1];
                        break;

                    case "zone":
                        card.zone = kv[1];
                        break;

                    case "zonePos":
                        card.zonePos = int.Parse(kv[1]);
                        break;
                }
            }

            var info = CardsInfo.GetCardInfo(card.cardId);

            if (info != null)
            {
                card.name = info.name;
                card.mana = info.mana;
            }

            return card;
        }

        static void searchCardsInBuffer(byte[] buffer, ref List<Card> results)
        {
            for (int i = 0; i < buffer.Length - 6; ++i)
            {
                if (
                    buffer[i + 0] == 0x63 && buffer[i + 1] == 0x61 &&
                    buffer[i + 2] == 0x72 && buffer[i + 3] == 0x64 &&
                    buffer[i + 4] == 0x49 && buffer[i + 5] == 0x64 &&
                    buffer[i + 6] == 0x3D
                    )
                {
                    try
                    {
                        var card = extractCard(buffer, i);

                        if (card != null)
                        {
                            results.Add(card);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        static public List<Card> GetHearthstoneCards(Process process)
        {
            List<Card> results = new List<Card>();

            SYSTEM_INFO sys_info = new SYSTEM_INFO();
            GetSystemInfo(out sys_info);

            IntPtr proc_min_address = sys_info.minimumApplicationAddress;
            IntPtr proc_max_address = sys_info.maximumApplicationAddress;

            long proc_min_address_l = (long)proc_min_address; //Mb cards addr always > 200mb //(long)proc_min_address;
            long proc_max_address_l = (long)proc_max_address;

            IntPtr processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_WM_READ, false, process.Id);

            MEMORY_BASIC_INFORMATION mem_basic_info = new MEMORY_BASIC_INFORMATION();

            int bytesRead = 0;

            while (proc_min_address_l < proc_max_address_l)
            {
                VirtualQueryEx(processHandle, proc_min_address, out mem_basic_info, 28);

                if (mem_basic_info.Protect == PAGE_READWRITE && mem_basic_info.State == MEM_COMMIT)
                {
                    byte[] buffer = new byte[mem_basic_info.RegionSize];

                    ReadProcessMemory((int)processHandle, mem_basic_info.BaseAddress, buffer, mem_basic_info.RegionSize, ref bytesRead);

                    searchCardsInBuffer(buffer, ref results);
                }

                proc_min_address_l += mem_basic_info.RegionSize;
                proc_min_address = new IntPtr(proc_min_address_l);
            }

            return results;
        }
    }
}
