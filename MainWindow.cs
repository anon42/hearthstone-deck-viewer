﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace DeckViewer
{
    public partial class MainWindow : Form
    {
        class DeckCard
        {
            public string name;
            public int total = 0;
        };

        class Deck
        {
            public string name;
            public List<DeckCard> cards = new List<DeckCard>();
        };

        System.Timers.Timer processScanTimer;
        object timerSync = new object();
        bool? inGame = null;
        bool saveDeckChanges = true;

        object gameCardsSync = new object();
        Dictionary<int, Card> gameCards = new Dictionary<int, Card>();

        public MainWindow()
        {
            InitializeComponent();

            CardsInfo.Load();
            name.Items.AddRange(CardsInfo.cardNameList.ToArray());

            iniDecks();

            processScanTimer = new System.Timers.Timer(3000);
            processScanTimer.Elapsed += new ElapsedEventHandler(onProcessScan);
            processScanTimer.Enabled = true;
        }

        void iniDecks()
        {
            disableDeckGrid();

            var decks = loadDecks();

            deckComboBox.Items.Clear();
            deckComboBox.Items.AddRange(
                (from x in decks select x.name).ToArray()
                );

            if (decks.Count != 0)
            {
                showDeck(decks[0]);
                deckComboBox.Text = decks[0].name;
            }
        }

        void showDeck(Deck deck = null)
        {
            saveDeckChanges = false;
            deckGridView.Rows.Clear();
            saveDeckChanges = true;

            if (deck == null)
            {
                return;
            }

            foreach (var card in deck.cards)
            {
                try
                {
                    deckGridView.Rows.Add(new string[] { card.name, card.total.ToString(), CardsInfo.GetCardMana(card.name).ToString() });
                }
                catch (Exception)
                {
                }
            }

            updateCardCount();
        }

        List<Deck> loadDecks()
        {
            List<Deck> result = new List<Deck>();

            if (!File.Exists("./decks.json"))
            {
                using (StreamWriter sw = File.AppendText("./decks.json"))
                {
                    sw.WriteLine("[]");
                }
            }

            using (StreamReader r = new StreamReader("./decks.json"))
            {
                result = JsonConvert.DeserializeObject<List<Deck>>(r.ReadToEnd());
            }

            return result;
        }

        void saveCurrentDeck()
        {
            var currentDeckName = deckComboBox.Text;

            if (currentDeckName.Count() == 0 || !saveDeckChanges)
            {
                return;
            }

            Deck deck = new Deck();
            deck.name = currentDeckName;

            for (int i = 0; i < deckGridView.Rows.Count; ++i)
            {
                var cardName = deckGridView.Rows[i].Cells[0].Value;
                var cardTotal = deckGridView.Rows[i].Cells[1].Value;

                if (cardName == null || cardTotal == null)
                {
                    continue;
                }

                DeckCard card = new DeckCard();
                card.name = cardName as string;
                card.total = int.Parse(cardTotal as string);

                deck.cards.Add(card);
            }

            var decks = loadDecks();

            if ((from x in decks where x.name == currentDeckName select x).Count() == 0)
            {
                decks.Add(deck);
            }
            else
            {
                (from x in decks
                 where x.name == currentDeckName
                 select x).ToList().ForEach(x => x.cards = deck.cards);
            }

            var json = JsonConvert.SerializeObject(decks);

            using (var sw = new StreamWriter("./decks.json"))
            {
                sw.WriteLine(json);
            }
        }

        void onGameStart()
        {
            clearGameCards();

            inGameLable.Invoke((MethodInvoker)(
                () => inGameLable.Text = "yes")
                );
        }

        void onGameStop()
        {
            clearGameCards();

            inGameLable.Invoke((MethodInvoker)(
                () => inGameLable.Text = "no")
                );
        }

        static bool isInGame(List<Card> cards)
        {
            return (from x in cards where x.cardId.StartsWith("HERO_") select x).Count() == 2;
        }

        void checkGameStatus(List<Card> cards)
        {
            var curInGame = isInGame(cards);

            if (inGame == null || curInGame != inGame.Value)
            {
                inGame = curInGame;

                if (inGame.Value)
                {
                    onGameStart();
                }
                else
                {
                    onGameStop();
                }
            }
        }

        void clearGameCards()
        {
            lock (gameCardsSync)
            {
                gameCards = new Dictionary<int, Card>();
            }
        }

        void storeCard(Card card)
        {
            lock (gameCardsSync)
            {
                if (gameCards.ContainsKey(card.id))
                {
                    gameCards[card.id].zone = card.zone;
                    gameCards[card.id].zonePos = card.zonePos;
                }
                else
                {
                    gameCards[card.id] = card;
                }

                if (card.zone == "HAND")
                {
                    gameCards[card.id].my = true;
                }
            }
        }

        void updateCardList()
        {
            List<Card> myCards = new List<Card>();

            lock (gameCardsSync)
            {
                myCards = (from x in gameCards
                           where x.Value.my && x.Value.zone != "DECK"
                           select x.Value).ToList();
            }

            int count = 0;
            deckGridView.Invoke((MethodInvoker)(() => count = deckGridView.Rows.Count));

            for (int i = 0; i < count; ++i)
            {
                object cardNameObj = null;
                object cardTotalObj = null;

                deckGridView.Invoke((MethodInvoker)(() => cardNameObj = deckGridView.Rows[i].Cells[0].Value));
                deckGridView.Invoke((MethodInvoker)(() => cardTotalObj = deckGridView.Rows[i].Cells[1].Value));

                if (cardNameObj == null || cardTotalObj == null)
                {
                    deckGridView.Invoke((MethodInvoker)(() => deckGridView.Rows[i].Cells[3].Value = ""));
                    continue;
                }

                if (inGame != null && inGame.Value)
                {
                    string cardName = cardNameObj as string;
                    int cardTotal = int.Parse(cardTotalObj as string);

                    int left = cardTotal - (from x in myCards where x.name == cardName select x).Count();

                    deckGridView.Invoke((MethodInvoker)(() =>
                    {
                        if (deckGridView.Rows[i].Cells[3].Value as string != left.ToString())
                        {
                            deckGridView.Rows[i].Cells[3].Value = left.ToString();
                            deckGridView.Sort(deckGridView.Columns[3], ListSortDirection.Descending);
                            deckGridView.Refresh();
                        }
                    }
                        ));
                }
                else
                {
                    deckGridView.Invoke((MethodInvoker)(() => deckGridView.Rows[i].Cells[3].Value = ""));
                }
            }
        }

        void procCards(List<Card> cards)
        {
            checkGameStatus(cards);

            foreach (var card in cards)
            {
                storeCard(card);
            }

            updateCardList();
        }

        void onProcessScan(object source, ElapsedEventArgs e)
        {
            if (Monitor.TryEnter(timerSync))
            {
                try
                {
                    var processes = Process.GetProcessesByName("Hearthstone");

                    if (processes.Length != 1)
                    {
                        hearthstoneProcessStartedLable.Invoke((MethodInvoker)(
                            () => hearthstoneProcessStartedLable.Text = "no")
                            );

                        if (inGame == null || inGame.Value)
                        {
                            inGame = false;

                            onGameStop();
                        }

                        return;
                    }

                    hearthstoneProcessStartedLable.Invoke((MethodInvoker)(
                        () => hearthstoneProcessStartedLable.Text = "yes")
                        );

                    procCards(MemoryTools.GetHearthstoneCards(processes[0]));
                }
                finally
                {
                    Monitor.Exit(timerSync);
                }
            }
        }

        private void deckDeleteButton_Click(object sender, EventArgs e)
        {
            var decks = loadDecks();

            var json = JsonConvert.SerializeObject(
                (from x in decks where x.name != deckComboBox.Text select x).ToList()
                );

            using (var sw = new StreamWriter("./decks.json"))
            {
                sw.WriteLine(json);
            }

            iniDecks();
        }

        void disableDeckGrid()
        {
            deckGridView.Enabled = false;
            deckGridView.Visible = false;

            deckDeleteButton.Enabled = false;
        }

        void enableDeckGrid()
        {
            deckGridView.Enabled = true;
            deckGridView.Visible = true;

            deckDeleteButton.Enabled = true;
        }

        private void deckComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var decks = loadDecks();

            var selected = (from x in decks where x.name == deckComboBox.Text select x);

            if (selected.Count() == 0)
            {
                disableDeckGrid();
                showDeck();
                return;
            }

            enableDeckGrid();
            showDeck(selected.First());
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            string value = "";

            if (InputBox.Show("New deck", "Deck name:", ref value) != DialogResult.OK)
            {
                return;
            }

            if (value.Length == 0)
            {
                MessageBox.Show("Empty name", "Invalid deck name", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var decks = loadDecks();

            if ((from x in decks where x.name == value select x).Count() != 0)
            {
                MessageBox.Show("Deck already exist", "Invalid deck name", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            deckComboBox.Items.AddRange(new string[] { value });
            deckComboBox.Text = value;

            enableDeckGrid();
            saveCurrentDeck();
            updateCardCount();
        }

        private void deckGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && (e.ColumnIndex == 0 || e.ColumnIndex == 1))
            {
                saveCurrentDeck();
                updateCardCount();

                if (e.ColumnIndex == 0)
                {
                    var cardName = deckGridView.Rows[e.RowIndex].Cells[0].Value;

                    if (cardName != null && (cardName as string).Length != 0)
                    {
                        var mana = CardsInfo.GetCardMana(cardName as string).ToString();
                        deckGridView.Rows[e.RowIndex].Cells[2].Value = mana;
                    }
                }
            }
        }

        private void deckGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            saveCurrentDeck();
        }

        private void deckGridView_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells["count"].Value = "2";
        }

        void updateCardCount()
        {
            var decks = (from x in loadDecks() where x.name == deckComboBox.Text select x);

            if (decks.Count() == 0)
            {
                deckGroupBox.Text = String.Format("Deck");
                return;
            }

            int total = 0;

            foreach (var card in decks.First().cards)
            {
                if (card.name.Count() != 0)
                {
                    total += card.total;
                }
            }

            deckGroupBox.Text = String.Format("Deck ({0} cards)", total);
        }

        private void readMebutton_Click(object sender, EventArgs e)
        {
            new ReadMe().ShowDialog();
        }
    }
}
