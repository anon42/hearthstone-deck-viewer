﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeckViewer
{
    public class Card
    {
        public string name;
        public int id;
        public string cardId;
        public string zone;
        public int zonePos;
        public bool my = false;
        public int mana = 0;

        public override string ToString()
        {
            return String.Format("[id={0} name={1} zone={2} zonePos={3}]", id, name, zone, zonePos);
        }
    }
}
